#**********************************************
#
#		Chiffrement post-Quantique
#
#
#
#
# Project créé par  Alexandre Ansel
#                   Alexandre Azzaro
#                   Gulian Dragon
#
# Le 25/02/2020
#
# Programme de lancement des algorithmes de chiffrement
#
#**********************************************

import sys
from PySide2.QtWidgets import QMainWindow, QApplication, QListWidget, QListWidgetItem
from PySide2.QtCore import Qt, QModelIndex
from PySide2.QtGui import QIcon

from ChiffrementPostQuantiqueUI import Ui_MainWindow

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ajoutMethodeChiffrement(self.ui.tableauModeleExistant, QListWidgetItem("RSA"), QListWidgetItem("ECDSA"), QListWidgetItem("Sphincs+"))


        self.ui.tableauModeleExistant.itemDoubleClicked.connect(self.selectionMethodeChiffrementAdd)
        self.ui.tableauModeleSelectionne.itemDoubleClicked.connect(self.selectionMethodeChiffrementRemove)

        self.ui.boutonLancement.clicked.connect(self.debutBenchmark)

	# Retour de commande pour les boutons du menu
	#self.ui.menuFichier.triggered.connect(self.Methode)


	# Retour de commande pour les boutons de l'interface
	#self.ui.monBouton.clicked.connect(self.Methode)
	#self.ui.checkBox.stateChanged.connect(self.Methode)

    '''
    Méthode appelée lors d'un double clique sur un item de la liste des modèles existants
    '''
    def selectionMethodeChiffrementAdd(self):
        
        provenance = self.ui.tableauModeleExistant
        destination = self.ui.tableauModeleSelectionne

        itemSelectionne = provenance.currentItem()
        rowItem = provenance.indexFromItem(itemSelectionne)
        provenance.takeItem(rowItem.row())
        self.ajoutMethodeChiffrement(destination, itemSelectionne)
        
    '''
    Méthode appelée lors d'un double clique sur un item de la liste des modèles selectionnés
    '''
    def selectionMethodeChiffrementRemove(self):
        
        provenance = self.ui.tableauModeleSelectionne
        destination = self.ui.tableauModeleExistant

        itemSelectionne = provenance.currentItem()
        rowItem = provenance.indexFromItem(itemSelectionne)
        provenance.takeItem(rowItem.row())
        self.ajoutMethodeChiffrement(destination, itemSelectionne)
        
    '''
    Ajoute la methode de chiffrement à la table passée en paramètre
    '''
    def ajoutMethodeChiffrement(self, table, *elements):

        if type(table) != QListWidget:
            raise TypeError("Le type de table n'est pas un QListWidget")

        for element in elements:

            table.addItem(element)

    '''
    lancement du benchmark
    '''
    def debutBenchmark(self):
        nombreMethode = self.ui.tableauModeleSelectionne.count()
        methodeSeletionnee = list()
        for i in range(nombreMethode):
            methodeSeletionnee.append(self.ui.tableauModeleSelectionne.item(i).text())
            
        print(methodeSeletionnee)



if __name__ == "__main__":

    app = QApplication(sys.argv)

    window = MainWindow()

    window.show()

    sys.exit(app.exec_())
